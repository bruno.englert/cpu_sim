#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "cpu.h"




class Instruction: public Word{
protected:	
	int param0;
	int param1;
	int param2;
	bool konst;
public:
	Instruction(int _param0 ,int _param1, int _param2 , bool _konst):param0(_param0), param1(_param1),param2(_param2), konst(_konst){}
	virtual void exec(CPU& cpu)=0;

};

class Mov: public Instruction{
private:

public:
	Mov(int _param0 ,int _param1 , bool _konst):Instruction(_param0,_param1,0,_konst){}

	void exec(CPU& cpu){
		if (konst==true){
			cpu.write(param0,param1);
		}else{
			cpu.write(param0,cpu.read(param1));
		}
	}
};




class Add: public Instruction{
private:


public:
	Add(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

	void exec(CPU& cpu){
		if (konst==true){
			int sum = cpu.read(param1)+param2;
			cpu.write(param0, sum);
		}else{
			int sum = cpu.read(param1)+cpu.read(param2);
			cpu.write(param0, sum);
		}
	}
};

class Sub: public Instruction{
private:

public:
	Sub(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

	void exec(CPU& cpu){
		if (konst==true){
			int sub = cpu.read(param1)-param2;
			cpu.write(param0, sub);
		}else{
			int sub = cpu.read(param1)-cpu.read(param2);
			cpu.write(param0, sub);
		}
	}
};

class Mul: public Instruction{
private:

public:
	Mul(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

	void exec(CPU& cpu){
		if (konst==true){
			int mul = cpu.read(param1)*param2;
			cpu.write(param0, mul);
		}else{
			int mul = cpu.read(param1)*cpu.read(param2);
			cpu.write(param0, mul);
		}
	}



};

class Div: public Instruction{
private:

public:
	Div(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

	void exec(CPU& cpu){
		if (konst==true){
			int quotient = cpu.read(param1)/param2;
			cpu.write(param0, quotient);
		}else{
			int quotient = cpu.read(param1)/cpu.read(param2);
			cpu.write(param0, quotient);
		}
	}
};


#endif