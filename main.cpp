#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "instructions.h"
#include "cpu.h"
#include "memory.h"
#include "assembler.h"

#include "instructions.cpp"
#include "cpu.cpp"
#include "memory.cpp"
#include "assembler.cpp"

using namespace std;

#define DEBUG 0

int main ()
{
	Memory* memory = new Memory;
	CPU cpu(memory);

	try{
		load("code.txt", memory);

		for (int i = 0; i<999; ++i)	{
			#if DEBUG
			cout<<"pC:"<<cpu.getPC()<<"  R0:"<<cpu.read(0)<<"  R1:"<<cpu.read(1)<<"  R3:"<<cpu.read(3)<<"  R4:"<<cpu.read(4)<<"  R5:"<<cpu.read(5)<<endl;
			#endif
			cpu.execute();
			if(cpu.getPC()==16){
				cout<<cpu.read(0)<<endl;
			}
		}
	}catch(char const* e){
		cerr<<e<<endl;
	}

	delete memory;
	return 0;
}


/*	R0: jelenlegi szám
	R2: hány prim van eddig

	R3: jelenleg prím index
	R4: MOD eredménye

	memory:
	500->..: eddigi prímek 
*/