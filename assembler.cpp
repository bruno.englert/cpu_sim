#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>

#include "instructions.h"
#include "cpu.h"
#include "memory.h"
#include "assembler.h"

using namespace std;

#define DEBUG_ASSEMBLER 0


void removeCharsFromString( string &str, char* charsToRemove ) {
   for ( unsigned int i = 0; i < strlen(charsToRemove); ++i ) {
      str.erase( remove(str.begin(), str.end(), charsToRemove[i]), str.end() );
   }
}

int stringToInt(string const _input){
	stringstream str(_input);
	int val;
	str>>val;
	return val;
}

int checkInst(string inst){
	string valid_inst[]={"MOV", "ADD", "SUB", "MUL", "DIV", "MOD", "JMP", "JMPZ", "LOAD", "STORE", "invalid"};
	int i;
	for (i = 0; i < 10; i++)
		if(inst.compare(valid_inst[i])==0)
			break;
	
	if(i==10){
		throw "Invalid instruction";
	}

	return i;
}

void addInstructionToMemory(Memory *memory, string inst, int idx[4]){
	int idx1 = idx[0], idx2 = idx[1], idx3 = idx[2];
	bool flag = idx[3];

 	int sw = checkInst(inst);



 	switch (sw){
	case 0:
		memory->write(new Mov(idx1,idx2,flag)); 
		break;
	case 1:
		memory->write(new Add(idx1,idx2,idx3,flag));
		break;
	case 2:
		memory->write(new Sub(idx1,idx2,idx3,flag));
		break;
	case 3:
		memory->write(new Mul(idx1,idx2,idx3,flag));
		break;
	case 4:
		memory->write(new Div(idx1,idx2,idx3,flag));
		break;
	case 5:
		memory->write(new Mod(idx1,idx2,idx3,flag));
		break;
	case 6:
		memory->write(new Jmp(idx1,flag));
		break;
	case 7:
		memory->write(new JmpZ(idx1, idx2,flag));
		break;
	case 8:
		memory->write(new Load(idx1, idx2,flag));
		break;
	case 9:
		memory->write(new Store(idx1, idx2,flag));
		break;
	}
}

void processLine(string input, Memory* memory){
	stringstream str(input);

	int	param[4];
	string inst, tmp[3];

	str>>inst>>tmp[0]>>tmp[1]>>tmp[2];

	if(input.find("#")!=-1)
		param[3]=true;
	else
		param[3]=false;

	char chars[] = "R#";

	for (int i = 0; i < 3; ++i){
		removeCharsFromString(tmp[i], chars);
		param[i] = stringToInt(tmp[i]);
	}

	addInstructionToMemory(memory, inst, param);

	#if DEBUG_ASSEMBLER
	cout<<"param0: "<< param[0]<<", param1: "<<param[1]<<", param2: "<<param[2]<<", param3: "<<param[3]<<", INST: "<<inst<<endl;
	#endif
}

void load(char const *fileName, Memory* memory){
	cout<<"Loading file.. "<<endl;
 	string oneLine;
 	ifstream myfile(fileName);

  	if (myfile.is_open()){
   		while ( getline (myfile,oneLine)){
    		processLine(oneLine, memory);
    	}
    	myfile.close(); 
	}else{
	cout << "Unable to open  file"; 
	}

	cout<<"Finished loading file."<<endl;
}