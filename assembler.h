#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>

#include "instructions.h"
#include "cpu.h"
#include "memory.h"



void removeCharsFromString(std::string &str, char* charsToRemove);
int stringToInt(std::string const _input);
int checkInst(std::string inst);
void load(char const *fileName, Memory* memory);
void processLine(std::string input, Memory* memory);
void addInstructionToMemory(Memory *memory, int sw, int idx[4]);

#endif