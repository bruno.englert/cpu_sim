#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H


#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>

#include "memory.h"

class CPU;


class Instruction: public Word{
protected:	
	int param0;
	int param1;
	int param2;
	bool konst;
public:
	Instruction(int _param0 ,int _param1, int _param2 , bool _konst);
	virtual void exec(CPU& cpu)=0;
	virtual int getData();
	virtual void setData(int);
};


class Mov: public Instruction{
private:

public:
	Mov(int _param0 ,int _param1 , bool _konst);
	void exec(CPU& cpu);
};


class Add: public Instruction{
private:


public:
	Add(int _param0 ,int _param1, int _param2 , bool _konst);

	void exec(CPU& cpu);
};


class Sub: public Instruction{
private:

public:
	Sub(int _param0 ,int _param1, int _param2 , bool _konst);
	void exec(CPU& cpu);
};


class Mul: public Instruction{
private:

public:
	Mul(int _param0 ,int _param1, int _param2 , bool _konst);
	void exec(CPU& cpu);
};


class Div: public Instruction{
private:

public:
	Div(int _param0 ,int _param1, int _param2 , bool _konst);
	void exec(CPU& cpu);
};


class Mod: public Instruction{
private:

public:
	Mod(int _param0 ,int _param1, int _param2 , bool _konst);
	void exec(CPU& cpu);
};


class Jmp: public Instruction{
private:

public:
	Jmp(int _param0, bool _konst);
	void exec(CPU& cpu);
};


class JmpZ: public Instruction{
private:


public:
	JmpZ(int _param0, int _param1, bool _konst);
	void exec(CPU& cpu);
};


class Store: public Instruction{
private:

public:
	Store(int _param0, int _param1, bool _konst);
	void exec(CPU& cpu);
};


class Load: public Instruction{
private:

public:
	Load(int _param0, int _param1, bool _konst);
	void exec(CPU& cpu);
};

#endif