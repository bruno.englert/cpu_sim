#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>


#include "memory.h"
#include "cpu.h"

Memory::Memory(){
	for(int i=0; i<size_of; i++)
		data[i]=NULL;
	data_count=0;
}


Word* Memory::read(int idx){
	if(0<=idx && idx<size_of)
		if(data[idx]!=NULL)
			return data[idx];
		else
			throw "Memory read: NULL pointer!"; 
	else
		throw "Memory read: index ERR!";
}
void Memory::del(int idx){
	delete data[idx];
	data[idx]=NULL;
}
void Memory::write(Word* _data){
	if(0<=data_count && data_count<size_of){
		data[data_count]=_data;
		data_count+=1;
	}else{
		throw "Memory write: index ERR!";
	}
}	
void Memory::write_direct(Word* _data, int idx){
	if(0<=idx && idx<size_of)
		data[idx]=_data;
	else
		throw "Memory write_direct: index ERR!";
	
}	

Memory::~Memory(){
	for (int i = 0; i < size_of; ++i)
		delete data[i];
}



Data::Data(int _number):number(_number){}
void Data::setData(int _number){
	number=_number;
}
int Data::getData(){
	return number;
}
void Data::exec(CPU& cpu){
	throw "Data class: execution on Data class!";
}