#ifndef MEMORY_H
#define MEMORY_H

#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>

int const size_of=1024;

class Word;
class CPU;

class Memory{
private:
	Word* data[size_of];
	int data_count;
public:
	Memory();
	Word* read(int idx);
	void del(int idx);
	void write(Word* _data);
	void write_direct(Word* _data, int idx);
	~Memory();
};

class Word{
private:
public:
	virtual void exec(CPU& cpu)=0;
	virtual int getData()=0;
	virtual void setData(int)=0;
};

class Data: public Word{
private:
	int number;
public:
	Data(int _number);
	void setData(int _number);
	int getData();
	void exec(CPU& cpu);
};

#endif