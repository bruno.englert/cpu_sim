#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>

#include "instructions.h"
#include "memory.h"
#include "cpu.h"

Instruction::Instruction(int _param0 ,int _param1, int _param2 , bool _konst):param0(_param0), param1(_param1),param2(_param2), konst(_konst){}

int Instruction::getData(){
	throw "Instruction: getData on Instruction class!";
}
void Instruction::setData(int){
	throw "Instruction: setData on Instruction class!";
}



Mov::Mov(int _param0 ,int _param1 , bool _konst):Instruction(_param0,_param1,0,_konst){}

void Mov::exec(CPU& cpu){
	if (konst==true){
		cpu.write(param0,param1);
	}else{
		cpu.write(param0,cpu.read(param1));
	}
}



Add::Add(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

void Add::exec(CPU& cpu){
	if (konst==true){
		int sum = cpu.read(param1)+param2;
		cpu.write(param0, sum);
	}else{
		int sum = cpu.read(param1)+cpu.read(param2);
		cpu.write(param0, sum);
	}
}



Sub::Sub(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

void Sub::exec(CPU& cpu){
	if (konst==true){
		int sub = cpu.read(param1)-param2;
		cpu.write(param0, sub);
	}else{
		int sub = cpu.read(param1)-cpu.read(param2);
		cpu.write(param0, sub);
	}
}



Mul::Mul(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

void Mul::exec(CPU& cpu){
	if (konst==true){
		int mul = cpu.read(param1)*param2;
		cpu.write(param0, mul);
	}else{
		int mul = cpu.read(param1)*cpu.read(param2);
		cpu.write(param0, mul);
	}
}



Div::Div(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

void Div::exec(CPU& cpu){
	if (konst==true){
		int quotient = cpu.read(param1)/param2;
		cpu.write(param0, quotient);
	}else{
		int quotient = cpu.read(param1)/cpu.read(param2);
		cpu.write(param0, quotient);
	}
}



Mod::Mod(int _param0 ,int _param1, int _param2 , bool _konst):Instruction(_param0,_param1,_param2,_konst){}

void Mod::exec(CPU& cpu){
	if (konst==true){
		int modulo = cpu.read(param1)%param2;
		cpu.write(param0, modulo);
		
	}else{
		int modulo = cpu.read(param1)%cpu.read(param2);
		cpu.write(param0, modulo);
	}
}



Jmp::Jmp(int _param0, bool _konst):Instruction(_param0,0,0,_konst){}

void Jmp::exec(CPU& cpu){
	if (konst==true){
		cpu.setPC(param0-1);
	}else{
		cpu.setPC(cpu.read(param0)-1);
	}
}



JmpZ::JmpZ(int _param0, int _param1, bool _konst):Instruction(_param0,_param1,0,_konst){}

void JmpZ::exec(CPU& cpu){
	
		if (konst==true){
			if(param1==0)
			cpu.setPC(param0-1);
		}else{
			if(cpu.read(param1)==0)
			cpu.setPC(param0-1);
		}
}



Store::Store(int _param0, int _param1, bool _konst):Instruction(_param0,_param1,0,_konst){}

void Store::exec(CPU& cpu){
	if (konst==true){
		cpu.mem->write_direct(new Data(cpu.read(param0)),param1); 
	}else{
		cpu.mem->write_direct(new Data(cpu.read(param0)),cpu.read(param1)); 
	}
}



Load::Load(int _param0, int _param1, bool _konst):Instruction(_param0,_param1,0,_konst){}

void Load::exec(CPU& cpu){
	if (konst==true){
		cpu.write(param0, (cpu.mem->read(param1))->getData());
	}else{
		cpu.write(param0, (cpu.mem->read(cpu.read(param1)))->getData());
	}
}