#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>

#include "cpu.h"
#include "memory.h"


CPU::CPU(Memory* _mem):pc(0){
	mem=_mem;
}

int CPU::read(int idx){
	if(0<=idx && idx<16)
		return reg[idx];
	else
		throw "CPU read: Reg index ERR!";
}

void CPU::write(int idx, int _data){
	if(0<=idx && idx<16)
		reg[idx]=_data;		
	else
		throw "CPU write: Reg index ERR!";
}

void CPU::execute(){
	Word* line;
	line = mem->read(pc);
	line->exec(*this);
	pc+=1;
}

void CPU::setPC(int _pc){
	pc=_pc;
}
int CPU::getPC(){
	return pc;
}