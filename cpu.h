#ifndef CPU_H
#define CPU_H

#include <iostream>
#include "memtrace.h"
#include "memtrace.cpp"
#include <stdio.h>

#include "memory.h"

class CPU{
private:
	int pc;
	int reg[16];
public:
	Memory* mem;

	CPU(Memory* _mem);
	int read(int idx);
	void write(int idx, int _data);
	void execute();
	void setPC(int _pc);
	int getPC();
};

#endif